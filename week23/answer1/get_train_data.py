#coding:utf-8
def get_train_data(formulas_path,train_file_path,images_path):
    #formulas_path = "../../image2latex100k/im2latex_formulas.lst"
    #with open(formulas_path, 'r',encoding="latin_1") as f:
    with open(formulas_path, 'r') as f:
    #with open(formulas_path, 'r') as f:
        formulas = [line.rstrip('\n').strip(' ').split() for line in f.readlines()]
    #train_file_path="../../image2latex100k/im2latex_train.lst"
    with open(train_file_path,'r') as f:
        idx_imgname=[line.rstrip('\n').strip(' ').split() for line in f.readlines()]
    print(train_file_path)
    train_images=[]
    train_formulas=[]
    for i in range(1,len(idx_imgname)):
        # 处理前，week20,行号在前
        #formuolas_str=formulas[int(idx_imgname[i][0])]
        # 处理后，week21,行号在后
        formulas_str=formulas[int(idx_imgname[i][1])]
        train_formulas.append(formulas_str)
        #['\\widetilde\\gamma_{\\rm', 'hopf}\\simeq\\sum_{n>0}\\widetilde{G}_n{(-a)^n\\over2^{2n-1}}\\label{H4}']#
        # image's path
        #image_path = "../../image2latex100k/formula_images/"+idx_imgname[i][1]+".png"
        # 处理前，week20,行号在前,文件名在后，并且需要加后缀
        #image_path = images_path+idx_imgname[i][1]+".png"
        # 处理后，week21,行号在后,文件名在前，已经有后缀png,不需要另加
        image_path = images_path+idx_imgname[i][0]
        train_images.append(image_path)
        #print("[%s],%s,%s"%(i,formulas_str,image_path))
    return train_images,train_formulas

if __name__=="__main__":
    formulas_path="../../image2latex100k/im2latex_formulas.lst"
    train_file_path="../../image2latex100k/im2latex_train.lst"
    images_path="../../image2latex100k/formula_images/"
    images,formulas=get_train_data(formulas_path,train_file_path,images_path)
    print(len(images),len(formulas))
