# 工程代码调优日记
   
## 更新时间：20200709
   - 此版本代码，在训练4个epoch之后，loss由最开始的200左右，变为30多，batchsize = 32
   - 优化方法：sgd,没有正则化项，没有使用momentum, 
   - learning rate 为0.002
   - 编码器为单层lstm,没有使用attention
   - teacher force 为1

   还在继续训练中, 当前训练日志更新在：week23/train_log_20200709.log

## 更新时间： 20200710
   - 又继续训练2个epoch,loss 约在15左右，其它参数与0709版一样
   - 训练日志：week23/train_log_20200710.log

## 更新时间： 20200711
   - 找到导致bleu为零得原因了，训练数据组织错误，0时刻得输出应该gt[1],而不是gt[0]. 原程序设置为了gt[0],所以模型总是输入开始符，然后得到开始符
   - 添加了 norm_clip 


![输入图片说明](https://images.gitee.com/uploads/images/2020/0711/013515_58f58cd2_7401441.png "屏幕截图.png")

##  
| 代码 | epoch | loss | bleu | optim | log | 
| :-----| ----: | :----: | :----: |  :----: | :----: |
| baseline | 4 | 32 | bug | sgd,lr_0.002,tf_1| train_log_20200709.log | 
| baseline | 6 | 15 | bug | 同上 | train_log_20200710.log |



# bug 
- bleu一直为零，需解决[20200711版本代码尝试解决]

