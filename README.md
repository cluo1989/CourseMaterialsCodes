课程资料以及代码  
================  
## 导航目录
 - [仓库说明](#仓库说明)
 - [克隆仓库](#克隆仓库)
 - [代码提交](#仓库说明)
 - [文件目录结构](#文件目录结构)
 - [week17作业](#week17作业)
 - [week18作业](#week18作业)
 - [week19作业](#week19作业)
 - [week20作业](#week20作业)
 - [week21作业](#week21作业)
 - [week22作业](#week22作业)
 - [week23作业](#week23作业)
 - [week24作业](#week24作业)
 - [Modelarts使用效果](#Modelarts使用效果)
 - [Modelarts使用办法](#modelarts使用办法)
 - [更新日志](#更新日志)
 - [参与贡献](#参与贡献)




仓库说明  
--------  

除给班班提交作业代码之外，可以把自己的代码放在 github 上或者 gitee 上，github 连接慢的同学可以放在 gitee 上，gitee 的使用方法与 github 一样，但连接更快。  

克隆仓库  
--------  

```
git clone https://gitee.com/Mathematical_formula_recognition/CourseMaterialsCodes.git
```

代码提交  
--------  

```  
git add .
git commit -m "your content"
git push -u origin master
```  

文件目录结构  
--------  

- resources: 课程相关资料、电子书、论文等  
- utils: py 实用小工具  
- weekX: 第 X 周课程代码  

week17作业  
-----------  

### 作业要求  

![](https://images.gitee.com/uploads/images/2020/0415/193515_77327d62_7401441.png)  

![](https://images.gitee.com/uploads/images/2020/0415/193530_b3b6b256_7401441.png)  

### 优秀作业  

| 学员     | 仓库链接                   | 点评                                                         |
| -------- | -------------------------- | ------------------------------------------------------------ |
| txy14326 | https://gitee.com/txy14326 | 这个代码的思路特别清晰。训练一个模型主要的步骤都有：样本数据准备，gt 的准备，模型的建立，loss 函数的定义，优化函数的选择等等。<br>在这个思路上扩充成大规模的数据集，大模型会比较容易，比较好理解。 |

![](https://images.gitee.com/uploads/images/2020/0418/123806_80214156_7401441.png)  

week18作业  
-----------  

### 作业要求  

![](https://images.gitee.com/uploads/images/2020/0418/125454_7a298afa_7401441.jpeg)

作业建议步骤以及建议代码：  

1. 数据准备（图片读取，对应的GT生成)[数据这个没有讲，所以代码直接提供，不用填空，代码位置：week18/dataset.py]  
2. EAST 模型建立[填空代码week18/model.py]  
3. loss 函数建立[填空代码week18/loss.py]  
4. 优化函数，超参数选择[填空代码week18/train.py]  
    
> 开始作业前，建议先下载 VGG 模型预训练权重 pths.zip 和数据集 ICDAR_2015.zip。  
>
> ​    [ICDAR_2015.zip](https://pan.baidu.com/s/14f44grwKT8rAauRR1c2InA)，密码：hc1v
>
> ​    [pths.zip](https://pan.baidu.com/s/1acCEDRipkT7FOJ1wlO0Eyg)，密码：108d  
>
> 下载后解压，将 `ICDAR_2015` 和 `pths` 文件夹放入 `week18` 目录中。  

### week18作业答案
  答案并不惟一，也不一定是最优秀的
  1. 对应答案已上传到week18/loss_0509.py week18/train_0509.py week18/model_0509.py


### 数学公式检测数据标注
  数学公式识别检测数据集：  
  [cvpr2019](https://pan.baidu.com/s/100OAXTIOTPoMjbi-dwOcxA)，提取码：0sbm  
  [cvpr2020](https://pan.baidu.com/share/init?surl=lo3smbFWiBSNnut9JssYaQ)，提取码：8171  

进入标注小组办法：
  - 这里是列表文本在modelarts平台上标注有账号，
  - 将modelarts绑定的邮箱发给老师，老师将你加入标注小组中。
  - 成功加入标注小组后，你会收到如下邮件：
      ![输入图片说明](https://images.gitee.com/uploads/images/2020/0429/170329_d5beb2de_7401441.png "屏幕截图.png")
 
标注办法：
  1. 点开邮件里面的链接，登陆并更改初始密码，登陆进去 标注系统
  2. 对于含有公式的图片，进行如下标注：
      ![输入图片说明](https://images.gitee.com/uploads/images/2020/0429/164600_2dc1bd5e_7401441.png "标注办法_有公式.png")
  3.  遇到不含有公式的图片，进行如下标注：
     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0429/164531_2840d9c0_7401441.png "标注办法_没有公式.png")
     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0503/195422_60f92ce5_7401441.png "屏幕截图.png")


week19作业  
-----------  

### 作业要求 
[计划20200427日晚更新完毕]  
1. 完成locality-aware NMS
2. 完成EAST模型的前向计算过程

 作业建议步骤以及建议代码：
1. /week19/inference.py 88行，独立完成locality-aware NMS
2. 完成EAST模型的前向计算过程，输入一张图片，输出其相应结果。
 
 可选作业：
 1. 完成模型的评价测试工作：计算f-socre/h-mean.


### 作业参考答案
  1. locality-aware-NMS 答案在week19/locality_aware_nms.py
  2. week19/pths下提供了两个模型文件，best.pth与 epoch_10.pth
       - best.pth是训练好的模型，inference.py 调用它后，可得到 week19/res_best.bmp
       - epoch_10.pth是训练到第10个epoch时的模型，inference.py 调用它后，可得到 week19/res_epoch_10.bmp
  3. 可选作业答案：
 ```
  1. 参考代码位置：week19/get_f1_score.py
      python get_f1_score.py 可以看到例子
  2. 关于f-score 意义的理解可以参考这里：https://zhuanlan.zhihu.com/p/141033837
 ```

week20作业
-----------
### 作业要求
 1. 完成dataloader.py

> 
 
 作业建议步骤以及建议代码
   1. 代码位置：week20/dataloader_0511.py,需要填空的地方：72行， 97行。 
   2. 72行是实现公式字符串转换为向量的函数:formulas2tensor
   3. 97是实现公式字符串和公式的图片对应起来的函数，这个主要参考数据集上的readme.
   4. 填写完成后，运行python data_loader_0511.py,得到如下结果为正确：
     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0511/163558_6d618331_7401441.png "屏幕截图.png") 
    
> 开始作业前，从这里下载数据集image2latex100k.zip  
>
>     2020522之后一周有效:
>     链接:https://pan.baidu.com/s/1CM9spFSXkyzkc4LB76YNPA  密码:ubtm
>
>    关于数据生成思路，可以在这里看看能否收到启发：
>    - 数据集的介绍：https://zhuanlan.zhihu.com/p/139632653
>    - 数据生成的结果示范：https://zhuanlan.zhihu.com/p/139693219
    
week21作业
-----------------
    1. 完成im2latex模型的前向计算程序inference.py,参考代码位置：week21/inference_0520.py

   作业建议步骤以及建议代码
   1. 代码位置：week21/inference_0520.py,需要填空的地方：46行， 62行, 89行. 
   2. 46行是实现编码器的计算过程
   3. 62是将cnn输出的feature map 转话为 编码器需要的数据格式 成序列的向量，难点时我们还有batch在，要注意。
   4. 89行时实现解码器的计算过程，并实现输出概率p的计算过程。概率是要求在hidden的基础上再使用一个神经网络单元进行计算。

```
    week21参考答案：week21/inference_20200527.py
    与inference_20200527配套代码：
            week21/data_loader.py
            week21/get_train_data.py
```


week22作业
-----------------------------


    1. 完成数据的重新整理（为了尽快让大家训练起来，这里我先提供一份，后续把数据整理代码发出来，数据整理过程确实有点繁琐）
    2. 完成训练程序（相比inference,训练需要添加loss,然后backward,这些是这个项目的真正难点）
    3. 完成基本流程后，需要添加teacher forcing,以看到收敛效果。
    4. 总体思路：完成代码后，先完成小批量数据在cpu上的训练，添加后续所有组件，然后再转入gpu,进行大批量数据训练。
   
           小批量数据训练，验证了我们写的训练代码可以基本闭环，是有效的，为后续大批量数据训练提供基础。

    5. 后续需要添加的组件有：[schedule sampling], [row encoder and position embedding], [word embedding],这些组件，我会最近尽快更新上去。

小批量数据训练，验证了我们写的训练代码可以基本闭环，是有效的，为后续大批量数据训练提供基础。
小批量训练效果展示：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0529/101549_0838b3a6_7401441.png "屏幕截图.png") 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0529/102635_33b9c79e_7401441.png "屏幕截图.png")


```
  作业建议步骤以及建议代码
  1. 数据位置：week22/im2latex_100K_norm_data_split_100M，
       使用方法：
             a 此文件夹内含有6个文件都是以x开头，每个大约90M，将其传入ModelArts 的notebook上使用【点击notebook 页面 upload】
             b 将x开头的文件，同步到teminal里
             c 执行cat_unzip.sh 里面的命令，即可完成数据准备。
  2. 待填空代码位置：week22/train_20200529.py 需要填空的地方在385,193,126,325行。
  3. 385行是实现loss函数的定义，这个函数在193行要使用。
  4. 126行是 随机梯度下降的内容，需要你完成权重的数值的更新。
  5. 325行需要实现teacher forcing功能
  6. 这份代码我在dataloader中做了设置：只训练2张图片，batch_size=2,epoch每次都更新。
  7. 当上传到gpu训练时，需要使用data_loader_whole.py 代替data_loader.py即可。
  
```  
week23作业
-------------------------------------
    1. 先用 BLEU 来评价自己train的model(少量图片)得BLEU,验证训练得闭环性。
    2. 验证完成后，使用大量图片进行训练，这时候需要改小学习率，或者使用clip_gradients方法。
    3. 使用attnetion结构[dot,product]
    4. 使用双lstm结构

```
    作业建议步骤以及建议代码
    1. 先用baseline模型，训练几个epoch, 查看收敛情况。week23/answer1中为训练代码。week23/pth/中模型文件为训练了4个epoch的模型参数文件。
    2. 更换sgd 为momentum + L2正则化
    3. 使用双lstm结构
    4.  使用 attention结构
    5. 使用clip_gradients方法
    6. 使用词向量
    7.使用multi-head attention
```
week24作业
-------------------------------------
     1.将我们训练得到的两个模型：east和im2latex部署到webserver上。
     
```
   作业建议步骤以及建议代码
   1.参考模型部署的工程代码：https://gitee.com/anjiang2020_admin/bd_cv3_webserver_demo
```


Modelarts使用效果
-------------------

gpu类型：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/201206_106659bb_7401441.png "tesla_v100_32g.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/201244_1ed05270_7401441.png "modelarts_terminal.png")


训练速度特别快，数万数据，一个epoch只要45秒左右。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/201219_e3c1b5f1_7401441.png "训练过程2.png")



Modelarts使用办法
-------------------

第一次配置时间较长，建议腾出至少4个小时的时间来配置。
使用云上的独立GPU，注意一点：obs是你唯一可以保存文件的地方，其他地方的保存都是暂时的，会清除。
 pipeline:
 1. 准备好自己的gitee
 2. ~~建立modelarts上的notebook~~
 3. 打开notebook对应的terminal
 4. git clone you_program 到terminal里
 5. 利用notebook同步功能将数据同步到terminal里
 6. 数据和代码都有了，可以配置环境和开发了。
 7. 处理的任务步骤中，都可以同步文件到obs上，同步代码：/week19/upload.py）
     

下面是详细步骤：

1. 准备好自己的gitee

2. 建立modelarts上的notebook【建议建立一个6小时的】

创建notebook
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200731_7a27883e_7401441.png "创建notebook.png")

notebook参数
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200747_31ff1e4c_7401441.png "创建notebook的参数.png")

notebook参数
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200812_a36a2930_7401441.png "创建notebook的参数1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200821_bc7f574f_7401441.png "创建notebook的参数3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200830_148d5177_7401441.png "创建notebook的参数5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200843_40baf02c_7401441.png "确认notebook的参数1.png")

从这里打开我们刚才创建的notebook环境
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200900_c8927593_7401441.png "打开notebook.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200911_9e960111_7401441.png "打开我们刚才创建的notebook.png")



3. 打开notebook对应的terminal
         
打开terminal
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/195637_5a27d243_7401441.png "打开teminal.png")

在terminal里自己可以随意操作
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/195655_c3af4a7b_7401441.png "已经打开的terminal.png")
        
termial使用原则
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/195713_d68cf58d_7401441.png "terminal的使用办法.png")

4. git clone 你的gitee，把代码放到terminal里
     查看README文件，切换到pytorch环境，可以使用gpu

5. 利用notebook的同步功能，把数据也同步到terminal里

选中notebook里的文件，点击syn,可以将notebook里的文件同步到terminal里
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/195747_a4e06f57_7401441.png "obs同步到terminal.png")

6. 数据和代码都有了，可以配置环境和开发了。

7. 任何时候，都可以同步文件到obs上，可以参考/week19/upload.py，如下图：
     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0503/195712_b05ad47a_7401441.png "屏幕截图.png")
    
  

TO DO LIST:  

1. 待添加标注工具【已完成】  
2. 待标注数学公式数据[已完成]  
3. 解决loss的其中一个设计问题：负样本loss返回零
4. 收集学员modelarts绑定的邮箱，分配标注任务【标注中】。

更新日志  
-------- 
20200529:
1. 添加week23作业。loss设计，反向传播等。
2. 添加word embedding [待添加]
3. 添加 行编码 与 position embedding [待添加]
4. 添加 teacher forcing
5. 添加 schedule sampling [待添加]

20200527:
1. 添加week21作业:inference得答案。
20200522:
1. 更新im2latex100k下载链接
20200520:
1. 添加week21作业
20200511:
1. 添加week20作业

20200509:
1. 添加week19作业答案：locality_aware_nms.py
2. 添加week18作业答案：loss_0509.py model_0509.py train_0509.py 
3. 上传 best.pth,epoch_10.pth,以及其对应的示例图片

20200503:

1. 添加obs使用办法
2. 更改数据生成时的bug.感谢@郑万铎同学
      ![输入图片说明](https://images.gitee.com/uploads/images/2020/0503/205825_78b76bb7_7401441.png "屏幕截图.png")


20200429:
1. 添加数学公式检测数据集，团队标注办法

20200428:
1. 添加modelarts使用办法


20200425:
1. 添加EAST的论文精读翻译
2. 添加EAST论文的课堂讲解
3. 添加EAST论文得作业代码inference.py

20200422:  

1. 调整目录结构  
2. 优化 README.md  

20200421:  

1. 实验添加 pdf 转图片代码
2. 整理大量 pdf 论文下载链接 


参与贡献  
--------  

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

码云特技  
--------  

1. 使用 README\_XX.md 来支持不同的语言，例如 README\_en.md, README\_zh.md  
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)  
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目  
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目  
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)  
6. 码云封面人物是一档用来展示码云会员风：采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)  
